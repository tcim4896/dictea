/* TOOLS */
cl = console.log;
px = function(val) {return typeof val=="string"?parseInt(val):(val += "px");};
rgba = function(){let r="rgba(";for(let i=0;i<arguments.length;i++){if(i<4){r+=arguments[i]+=i!==3?", ":")";}}return r;}
/* CONSTANTS */
const absolute="absolute";
const fixed="fixed";
const relative="relative";
const none="none";
const click="click";
const keydon="keydown";
const mousedown="mouseup";
/* DUMMY */
let ipsum="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",text=ipsum;
/* FUNC */
var mStateMap = {
  applied:[]
};

function register(id,element){
  mStateMap[id]=element;
}

function select(id){
  return mStateMap[id];
}s=select;

localStorage.clear();
localStorage.setItem("words", JSON.stringify([]));
localStorage.setItem("text", JSON.stringify(""));

function store(word) {
  var words = JSON.parse(localStorage.getItem("words"));
  words.push(word);
  localStorage.setItem("words", JSON.stringify(words));
}

function getList(char) {
  let r = [],
    words = JSON.parse(localStorage.getItem("words"));
  for (let word of words) {
    word[0].toLowerCase() == char.toLowerCase() ? r.push(word) : {};
  }
  return r;
}

function getAppliedMethods(){
  return mStateMap["applied"];
}
// if string1 defined; 
// * add apply(not declared) [replace:a,undefined]
// * if string1 string2 defined
// * add apply(not declared) [replace:a,b]
// * apply button pressed: add method to applied stack
// * clear input field: remove temporar apply from stack

function replace(string1,string2){
    // not finished yet
    var r=[],
        blen=string1.length,
        slen=string2.length;
    return function init(source){
      for(let a=0;a<slen;a++){
        var match=true;
        for(let b=0;b<blen;b++){
          if(string1[b]!==source[a+b]){
            match=false;
          }
          if(b==blen-1&&match==true){
            r.push({a,blen})
          }      
        }
      } // detecting string1
      let exclude=[];
      for(let data of r){
        for(let i=0;i<data.a+data.blen-1;i++){
          exclude.push(data.a+i) // generating list of indexes where the replacement starts
        }
      }
      return source.split('').map((char,idx)=>{
        return exclude.indexOf(idx)!==-1?string2:char;
      }).join('');
    };
}

function excludeOne(char,text){
    var r="";
    for(let i=0;i<text.length;i++){
      text[i]!=char?
      (r+=text[i]):0;
    }
    return r; 
}

function filter(chars,source){
  var r=[],
      blen=chars.length,
      slen=source.length,
      output="";
  for(let a=0;a<slen;a++){
    var match=true;
    for(let b=0;b<blen;b++){
      if(chars[b]!==source[a+b]){
        match=false;
      }
      if(b==blen-1&&match==true){
        r.push({a,blen})
      }      
    }
  }
  let exclude=[];
  for(let data of r){
    for(let i=0;i<data.a+data.blen-1;i++){
      exclude.push(data.a+i)
    }
  }
  // overkill
  output=source
    .split('')
    .filter(function(char,idx){
      return exclude.indexOf(idx)==-1;
  }).join('');
  return output;
}

function apply(data) {
  let id="apply";data.method?mStateMap.applied.push({
    type:data.type,
    method:data.method
  }):{};
}

function applyMethods(source,i=0){
  for(let data of mStateMap.applied){
    cl(i);i++;
    source=data.method(source);
  }
  return source;
}

function filterChar(character){
    return function(string){
        var r="";
        for(let char of string){
            if(char!==character){
                r+=char;
            }
        }
      return r;
    }
}

function bitscan(source, term) {
  var r = [],
    slen = source.length;

  function conv(text) {
    let elm = span.cloneNode(true);
    elm.textContent = text;
    return elm;
  }

  for (let a = 0; a < slen; a++) {
    if (source[a] == "term") {
      r.push(conv(source[a]));
    } else {
      r.push(source[a]);
    }
  }
  return r;
}

localStorage.setItem("text", JSON.stringify(ipsum));

store("profound");
store("adjective");
store("outburst");
store("anger");
store("agression");
store("expression");

/* UI */
let 
div = document.createElement("div"),
ul = document.createElement("ul"),
li = document.createElement("li"),
span = document.createElement("span"),
container = div.cloneNode(true),
list = ul.cloneNode(true),
input = document.createElement("div"),
checkbox = document.createElement("div"),
textarea = div.cloneNode(true),
button = div.cloneNode(true);

styler(button,{
  width: "auto",
  height: px(40),
  border: "1px solid black",
  borderRadius: px(4),
  backgroundColor: "#666",
  textAlign:"center"
})

function Component(identifier,type){
  let component;
  switch(type){
    case "div":
      component = div;
      mStateMap[identifier]=component;
    break;
    case "ul":
      component = ul;
      mStateMap[identifier]=component;      
    break;
    case "li":
    break;
    case "button":
      component = button.cloneNode(true);
      component["id"]=identifier
      mStateMap[identifier]=component;
    break;
    case "textarea":
      component = textarea.cloneNode(true);
      component["id"]=identifier
      mStateMap[identifier]=component;
    break;
    case "input":
      component = input.cloneNode(true);
      component["id"]=identifier;
      mStateMap[identifier]=component;
    break;
    case "checkbox":
      component = checkbox.cloneNode(true);
      component.addEventListener("click", function(e){
        if(component.checked==true){
          component.checked=false;
          component.textContent="";
        }else{
          component.checked=true;
          component.textContent="*";
        }
      })
      component["id"]=identifier;
      mStateMap[identifier]=component;
      mStateMap[identifier].checked=false;
    break;
    case "container":
      component = container.cloneNode(true);
      styler(component,{
        position: absolute,

      })
      addProp(component, "class","container");
      component["id"]=identifier;
      mStateMap[identifier]=component;
      mStateMap[identifier].checked=false;
    break;
    case "span":
    break;
    default:
    break;
  }
  return component;
}

function Li(text, className) {
  var nw = li.cloneNode(true);
  nw.textContent = text;
  if (className) {
    nw.setAttribute("class", className);
  }
  return nw;
}

function styler(component,styles){
  for(let style in styles){
    component[style]=styles[style];
  }
}

function addProp(component,propName,propValue){
  component.setAttribute(propName,propValue)
  return component;
}

function addProps(component,props){
  for(let prop of props){
    addProp(component,prop.type,prop.value)
  }
  component;
}

sidebar=Component("sidebar","container")
content=Component("content","container")
Component("a","container")
b=Component("b","container")
c=Component("c","container")
d=Component("d","container")

styler(select("a"),{
  position: absolute,
  left:px(5),
  width:px(400),
  height:px(100),
  backgroundColor: rgba(0,0,0,0.5),
})

/* experimental */
function stateChange(){
  toD(select("methods0"),getAppliedMethods().map(data=>Li(data.type,"item")),true)
  toD(select("textarea0"),applyMethods(ipsum),true)
}

function event(component,event){
  cl("Added an event to",component.id)
  component.addEventListener(event.type, function(e){
    event.fn(component,e);
    stateChange(event.type,event.method)
  })
}

function styler(component,styles){
  for(let style in styles){
    component.style[style]=styles[style];
  }
}

/* object generation */
addProps(Component("input0","input"),[{type:"class",value:"input"}])
addProps(Component("input1","input"),[{type:"class",value:"input"}])
addProps(Component("checkbox0","checkbox"),[{type:"class",value:"checkbox"}])

Component("textarea0","textarea")
Component("button0","button")

event(select("input0"),{
  type: "keydown",
  fn: function(elm,e){
    cl(elm.innerText)
  }
})

event(select("input1"),{
  type: "keydown",
  fn: function(elm,e){
    cl(elm.innerText)
  }
})

styler(select("checkbox0"),{ //chaining.................................................
  position: absolute,
  width: px(20),
  height: px(20),
})

event(select("button0"),{
  type: "mousedown",
  fn: function(elm,e){
    apply({type:"replace",method:(text)=>excludeOne("e",text)})
  }
})

b.style.position = "absolute"; /* converter for this code? */
b.style.left = px(410);
b.style.width = px(400);
b.style.height = px(200);
b.style.background = "rgba(0,0,0,0.5)";

Component("methods0","ul")
addProp(select("methods0"),"class","list")

c.style.position = "absolute";
c.style.top = px(105);
c.style.left = px(5);
c.style.width = px(400);
c.style.height = px(400);
c.style.background = "rgba(0,0,0,0.5)";

d.style.position = "absolute";
d.style.top = px(205);
d.style.left = px(410);
d.style.width = px(400);
d.style.height = px(200);
d.style.background = "rgba(0,0,0,0.5)";

sidebar.setAttribute("class", "sidebar");
content.setAttribute("class", " content");

var main = [s("sidebar"), s("content")];
m=mStateMap;
m.sidebar;
toD(select("a"), 
  ["filter:", 
    select("input0"),
    select("input1"), 
    select("checkbox0"),
    select("button0"), 
  ]);
toD(select("b"),["applied methods", select("methods0")]);
toD(select("c"),[select("textarea0")]);
toD(select("d"),["used"]);
toD(select("sidebar"), getList("a").map(item => Li(item, "item")));
toD(select("content"), ['a', 'b', 'c', 'd']);
toD(document.body,main);

/* MAIN BUILD SCRIPT */

function toD(base, seq, override) {
  // <-- use select here and accept strings?
  // no we'd like to add strings too....?
  if(override){
    base.innerHTML='';
  }
  for (let val of seq) { // hwat ewfi wntu usz quott for cmpmpinentos gen when nto defined
    if (typeof val=="string") {
      let string= document.createTextNode(true);
      string.textContent=val;
      base.appendChild(mStateMap[val]?select(val):string);
    }else{
      base.appendChild(val);
    }
  }
}