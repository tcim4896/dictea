cat ./* >> ./dist/main.js<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <script src="main.js"></script>
</body>
</html>/* TOOLS */
cl = console.log;
px = function(val) {
  return typeof val == "string" ? parseInt(val) : (val += "px");
};
rgba = function(){
  let r="rgba(";
  for(let i=0; i<arguments.length;i++){
    if(i<4){
      r+=arguments[i]+=i!==3?", ": ")";
    }
  }
  return r;
}

/* DUMMY */
let ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

/* FUNC */
var mStateMap = {
  applied:[]
};

function stateChange(){
  cl("StateChange",mStateMap.applied); //check mStateMap for change?
  for(let method of mStateMap.applied){
    select("textarea0").textContent=method(select("textarea0").textContent)
  }
  
}

function register(id,element){
  mStateMap[id]=element;
}

function select(id){
  return mStateMap[id];
}

localStorage.clear();
localStorage.setItem("words", JSON.stringify([]));
localStorage.setItem("text", JSON.stringify(""));

function store(word) {
  var words = JSON.parse(localStorage.getItem("words"));
  words.push(word);
  localStorage.setItem("words", JSON.stringify(words));
}

function getList(char) {
  let r = [],
    words = JSON.parse(localStorage.getItem("words"));
  for (let word of words) {
    word[0].toLowerCase() == char.toLowerCase() ? r.push(word) : {};
  }
  return r;
}

function getAppliedMethods(){
  return mStateMap["applied"];
}

function replace(string1,string2,source){
    var r=[],
        blen=string1.length,
        slen=string2.length,
        output="";
    for(let a=0;a<slen;a++){
      var match=true;
      for(let b=0;b<blen;b++){
        if(string1[b]!==source[a+b]){
          match=false;
        }
        if(b==blen-1&&match==true){
          r.push({a,blen})
        }      
      }
    }
    let exclude=[];
    for(let data of r){
      for(let i=0;i<data.a+data.blen-1;i++){
        exclude.push(data.a+i)
      }
    }
    output=source.split('');
    exclude.forEach(function(idx){
     output[idx]=string2; 
    });
}

function exclude(str,text){
    var r="";
    for(let i=0;i<text.length;i++){
        for(let x=0;x<str.length;x++){
            if(char[x]==str[i]){
                r+=(str[i]);
            }
        }

    }
    return r; // ?
}

function filter(chars,source){
  var r=[],
      blen=chars.length,
      slen=source.length,
      output="";
  for(let a=0;a<slen;a++){
    var match=true;
    for(let b=0;b<blen;b++){
      if(chars[b]!==source[a+b]){
        match=false;
      }
      if(b==blen-1&&match==true){
        r.push({a,blen})
      }      
    }
  }
  let exclude=[];
  for(let data of r){
    for(let i=0;i<data.a+data.blen-1;i++){
      exclude.push(data.a+i)
    }
  }
  // overkill
  output=source
    .split('')
    .filter(function(char,idx){
      return exclude.indexOf(idx)==-1;
  }).join('');
  return output;
}

function apply(method) {
  method?mStateMap["applied"].push(method):{};
  stateChange();
}
  
function filterChar(character){
    return function(string){
        var r="";
        for(let char of string){
            if(char!==character){
                r+=char;
            }
        }
      return r;
    }
}

function bitscan(source, term) {
  var r = [],
    slen = source.length;

  function conv(text) {
    let elm = span.cloneNode(true);
    elm.textContent = text;
    return elm;
  }

  for (let a = 0; a < slen; a++) {
    if (source[a] == "term") {
      r.push(conv(source[a]));
    } else {
      r.push(source[a]);
    }
  }
  return r;
}

localStorage.setItem("text", JSON.stringify(ipsum));

store("profound");
store("adjective");
store("outburst");
store("anger");
store("agression");
store("expression");

/* UI */
let div = document.createElement("div");
let ul = document.createElement("ul");
let li = document.createElement("li");
let span = document.createElement("span");

let input = document.createElement("div");
input.setAttribute("class", "input");
input.setAttribute("contentEditable", "true");

let checkbox = document.createElement("div");
checkbox.setAttribute("class", "checkbox");

let textarea = div.cloneNode(true);
textarea.setAttribute("contentEditable", "true");
textarea.setAttribute("spellcheck", "false");
textarea.textContent=ipsum;
textarea.setAttribute("class", "textarea");

let button = div.cloneNode(true);
styler(button,{
  width: "auto",
  height: px(40),
  border: "1px solid black",
  borderRadius: px(4),
  backgroundColor: "#666",
  textAlign:"center"
})

function Component(identifier,type){
  let component;
  switch(type){
    case "div":
      component = div;
      mStateMap[identifier]=component;
    break;
    case "ul":
      component = ul;
      mStateMap[identifier]=component;      
    break;
    case "li":
    break;
    case "button":
      component = button.cloneNode(true);
      component["id"]=identifier
      mStateMap[identifier]=component;
    break;
    case "textarea":
      component = textarea.cloneNode(true);
      component["id"]=identifier
      mStateMap[identifier]=component;
    break;
    case "input":
      component = input.cloneNode(true);
      component["id"]=identifier;
      mStateMap[identifier]=component;
    break;
    case "checkbox":
      component = checkbox.cloneNode(true);
      component.addEventListener("click", function(e){
        if(component.checked==true){
          component.checked=false;
          component.textContent="";
        }else{
          component.checked=true;
          component.textContent="*";
        }
      })
      component["id"]=identifier;
      mStateMap[identifier]=component;
      mStateMap[identifier].checked=false;
    break;
    case "span":
    break;
    default:
    break;
  }
  return component;
}

function Li(text, className) {
  var nw = li.cloneNode(true);
  nw.textContent = text;
  if (className) {
    nw.setAttribute("class", className);
  }
  return nw;
}

function toD(base, seq) {
  // <-- use select here and accept strings?
  // no we'd like to add strings too....?
  for (let val of seq) {
    if (typeof val == "string") {
      let string= document.createTextNode(true);
      string.textContent=val;
      base.appendChild(string);
    } else {
      base.appendChild(val);
    }
  }
}


let list = ul.cloneNode(true);
let container = div.cloneNode(true);
container.setAttribute("class", "container");
let sidebar = container.cloneNode(true);
let content = container.cloneNode(true);
let a = container.cloneNode(true);
let b = container.cloneNode(true);
let c = container.cloneNode(true);
let d = container.cloneNode(true);
a.style.position = "absolute";
a.style.left=px(5);
a.style.width = px(400);
a.style.height = px(100);
a.style.background = "rgba(0,0,0,0.5)";

Component("input0","input")
Component("input1","input")
Component("checkbox0","checkbox")
Component("textarea0","textarea")
Component("button0","button")
select("button0").textContent="Apply";

event(select("input0"),{
  type: "keydown",
  fn: function(elm,e){
    apply(filterChar(e.key))
  }
})
event(select("button0"),{
  type: "keydown",
  fn: function(elm,e){
    apply(filterChar(e.key))
  }
})
function styler(component,styles){
  for(let style in styles){
    component.style[style]=styles[style];
  }
}
/* experimental */
const absolute="absolute";
const click="click";

styler(select("checkbox0"),{ //chaining.................................................
  position: absolute,
  width: px(20),
  height: px(20),

})

event(select("checkbox0"), {
  type: click,
  fn: function(component, e){
    cl("clicked", component)// selected component > stateChange > select(otherComponent);
    // sequence option; Array.isArray() ? apply1 : applyMultiple
  }
})

function event(component,event){
  component.addEventListener(event.type, function(e){
    event.fn(component,e);
  })
}

b.style.position = "absolute";
b.style.left = px(410);
b.style.width = px(400);
b.style.height = px(200);
b.style.background = "rgba(0,0,0,0.5)";

let methods=Component("methods0", "ul");
apply(function NAMDSA(){})
toD(methods,getAppliedMethods().map(item => Li(item.name, "item")))

c.style.position = "absolute";
c.style.top = px(105);
c.style.left = px(5);
c.style.width = px(400);
c.style.height = px(400);
c.style.background = "rgba(0,0,0,0.5)";

d.style.position = "absolute";
d.style.top = px(205);
d.style.left = px(410);
d.style.width = px(400);
d.style.height = px(200);
d.style.background = "rgba(0,0,0,0.5)";

sidebar.setAttribute("class", "sidebar");
content.setAttribute("class", " content");

var main = [sidebar, content];
toD(a, ["filter:", 
          select("input0"),
          select("input1"), 
          select("checkbox0"),
          select("button0"), 
        ]);  //<-- cons.
toD(b, ["applied methods", methods]);
toD(c, [select("textarea0")]);
toD(d, ["used"]);
toD(sidebar, getList("a").map(item => Li(item, "item")));
toD(content, [a, b, c, d]);
toD(document.body, main);
function scan(arr, i=0, r=[]){
    console.log(i);
    if(typeof arr[i]=="object"){
      if(Array.isArray(arr[i])){
        console.log("ArrayHandle")
        r.push(scan(arr[i]))
      }else{
        console.log("ObjectHandle")
        console.log(1,arr[i])
        r.push(iterationCopy(arr[i])
      }
    }else if(typeof arr[i]!=="undefined"){
      r.push(arr[i]);
    }
    
    if(typeof arr[i+1]!=="undefined"){
      scan(arr,i+1,r);
    }
    function iterationCopy(src) {
      let target = {};
      for (let prop in src) {
          if (src.hasOwnProperty(prop)) {
              if(typeof src[prop]=="object"){
                  if(Array.isArray(src[prop])){
                      console.log("ArrayHandle",src[prop])
                      target[prop] = scan(src[prop]);
                  }else{
                        console.log("ObjectHandle", src[prop])
                      target[prop] = iterationCopy(src[prop]);
                  }
              }else{
                  target[prop] = src[prop];
              }
          }
      }
      return target;
    }
    return r;
  }# DICTEA
## diffy's on reading

* replace words or characters
* filter words or characters
* 

# TODO
* write bundle script;cat/stdout(node)/html,body {
    margin:0;
    padding:0;
    height:100%;
  }
  
  [contentEditable=true]:empty:not(:focus):before{
      content:attr(data-text)
  }
  
  div {
    font-size: 13px;
  }
  
  .input {
    display: inline-block;
    height:20px;
    width:120px;
    background-color:#fff;
    border-radius:4px;
    border:1px solid #666;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.2) inset;
  }

  .checkbox {
    display: inline-block;
    width:20px;
    height:20px;
    border:1px solid rgba(0,0,0,0.8);
    background-color:rgba(0,0,0,0.2);
    user-select: none;
  }

  .sidebar{
    position:fixed;
    width: 200px;
    background-color: rgb(50, 87, 125);
    top: 0;
    bottom: 0;
    left: 0;
    transition: all .3s ease;
    box-shadow: inset -7px 0 50px -7px rgba(0,0,0,.5);
  }
  
  .item{
    line-height:40px;
    background-color: rgba(0,0,0,0.5);
    transition: ease-in-out 0.2s;
    padding: 5px;
    display:border-box;
    list-style:none;
    color: rgba(255,255,255,0.5);
  }
  
  .item:hover{
    background-color: rgba(0,0,0,0.2);
    cursor: pointer;
  }
  
  .content  {
    width: 100%;
    background-color: rgb(68, 125, 50);
    position: fixed;
    top: 0;
    bottom: 0;
    left: 200px;
    box-shadow: inset -7px 0 50px -7px rgba(0,0,0,.5);
  }
  
  .container {
    display:inline-block;
    padding: 5px;
    box-sizing: border-box;
  }
  
  span {
    display: inline-block;
  }
  
  .textarea {
    font-family: arial, sans-serif;
    font-size: 0.875em;
    background-color: #fff;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.2) inset;
    border-radius: 4px;
    padding: 5px;
    margin: auto;
    height:100%;
    box-sizing: border-box;
    height: 100%;
  }
  
  .methods {
    margin:0;
    padding:0;
  }

  .methods .item{
    display:inline-block;
    background-color: rgba(255,255,255,0.5);
    line-height: 14px;
    padding: 5px;
    margin: 5px 0px 5px 5px;
    border-radius: 4px;
    transition: ease-in-out 0.2s;
  }/* TOOLS */
cl = console.log;
px = function(val) {
  return typeof val == "string" ? parseInt(val) : (val += "px");
};
rgba = function(){
  let r="rgba(";
  for(let i=0; i<arguments.length;i++){
    if(i<4){
      r+=arguments[i]+=i!==3?", ": ")";
    }
  }
  return r;
}

/* DUMMY */
let ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

/* FUNC */
var mStateMap = {
  applied:[]
};

function stateChange(){
  cl("StateChange",mStateMap.applied); //check mStateMap for change?
  for(let method of mStateMap.applied){
    select("textarea0").textContent=method(select("textarea0").textContent)
  }
  
}

function register(id,element){
  mStateMap[id]=element;
}

function select(id){
  return mStateMap[id];
}

localStorage.clear();
localStorage.setItem("words", JSON.stringify([]));
localStorage.setItem("text", JSON.stringify(""));

function store(word) {
  var words = JSON.parse(localStorage.getItem("words"));
  words.push(word);
  localStorage.setItem("words", JSON.stringify(words));
}

function getList(char) {
  let r = [],
    words = JSON.parse(localStorage.getItem("words"));
  for (let word of words) {
    word[0].toLowerCase() == char.toLowerCase() ? r.push(word) : {};
  }
  return r;
}

function getAppliedMethods(){
  return mStateMap["applied"];
}

function replace(string1,string2,source){
    var r=[],
        blen=string1.length,
        slen=string2.length,
        output="";
    for(let a=0;a<slen;a++){
      var match=true;
      for(let b=0;b<blen;b++){
        if(string1[b]!==source[a+b]){
          match=false;
        }
        if(b==blen-1&&match==true){
          r.push({a,blen})
        }      
      }
    }
    let exclude=[];
    for(let data of r){
      for(let i=0;i<data.a+data.blen-1;i++){
        exclude.push(data.a+i)
      }
    }
    output=source.split('');
    exclude.forEach(function(idx){
     output[idx]=string2; 
    });
}

function exclude(str,text){
    var r="";
    for(let i=0;i<text.length;i++){
        for(let x=0;x<str.length;x++){
            if(char[x]==str[i]){
                r+=(str[i]);
            }
        }

    }
    return r; // ?
}

function filter(chars,source){
  var r=[],
      blen=chars.length,
      slen=source.length,
      output="";
  for(let a=0;a<slen;a++){
    var match=true;
    for(let b=0;b<blen;b++){
      if(chars[b]!==source[a+b]){
        match=false;
      }
      if(b==blen-1&&match==true){
        r.push({a,blen})
      }      
    }
  }
  let exclude=[];
  for(let data of r){
    for(let i=0;i<data.a+data.blen-1;i++){
      exclude.push(data.a+i)
    }
  }
  // overkill
  output=source
    .split('')
    .filter(function(char,idx){
      return exclude.indexOf(idx)==-1;
  }).join('');
  return output;
}

function apply(method) {
  method?mStateMap["applied"].push(method):{};
  stateChange();
}
  
function filterChar(character){
    return function(string){
        var r="";
        for(let char of string){
            if(char!==character){
                r+=char;
            }
        }
      return r;
    }
}

function bitscan(source, term) {
  var r = [],
    slen = source.length;

  function conv(text) {
    let elm = span.cloneNode(true);
    elm.textContent = text;
    return elm;
  }

  for (let a = 0; a < slen; a++) {
    if (source[a] == "term") {
      r.push(conv(source[a]));
    } else {
      r.push(source[a]);
    }
  }
  return r;
}

localStorage.setItem("text", JSON.stringify(ipsum));

store("profound");
store("adjective");
store("outburst");
store("anger");
store("agression");
store("expression");

/* UI */
let div = document.createElement("div");
let ul = document.createElement("ul");
let li = document.createElement("li");
let span = document.createElement("span");

let input = document.createElement("div");
input.setAttribute("class", "input");
input.setAttribute("contentEditable", "true");

let checkbox = document.createElement("div");
checkbox.setAttribute("class", "checkbox");

let textarea = div.cloneNode(true);
textarea.setAttribute("contentEditable", "true");
textarea.setAttribute("spellcheck", "false");
textarea.textContent=ipsum;
textarea.setAttribute("class", "textarea");

let button = div.cloneNode(true);
styler(button,{
  width: "auto",
  height: px(40),
  border: "1px solid black",
  borderRadius: px(4),
  backgroundColor: "#666",
  textAlign:"center"
})

function Component(identifier,type){
  let component;
  switch(type){
    case "div":
      component = div;
      mStateMap[identifier]=component;
    break;
    case "ul":
      component = ul;
      mStateMap[identifier]=component;      
    break;
    case "li":
    break;
    case "button":
      component = button.cloneNode(true);
      component["id"]=identifier
      mStateMap[identifier]=component;
    break;
    case "textarea":
      component = textarea.cloneNode(true);
      component["id"]=identifier
      mStateMap[identifier]=component;
    break;
    case "input":
      component = input.cloneNode(true);
      component["id"]=identifier;
      mStateMap[identifier]=component;
    break;
    case "checkbox":
      component = checkbox.cloneNode(true);
      component.addEventListener("click", function(e){
        if(component.checked==true){
          component.checked=false;
          component.textContent="";
        }else{
          component.checked=true;
          component.textContent="*";
        }
      })
      component["id"]=identifier;
      mStateMap[identifier]=component;
      mStateMap[identifier].checked=false;
    break;
    case "span":
    break;
    default:
    break;
  }
  return component;
}

function Li(text, className) {
  var nw = li.cloneNode(true);
  nw.textContent = text;
  if (className) {
    nw.setAttribute("class", className);
  }
  return nw;
}

function toD(base, seq) {
  // <-- use select here and accept strings?
  // no we'd like to add strings too....?
  for (let val of seq) {
    if (typeof val == "string") {
      let string= document.createTextNode(true);
      string.textContent=val;
      base.appendChild(string);
    } else {
      base.appendChild(val);
    }
  }
}


let list = ul.cloneNode(true);
let container = div.cloneNode(true);
container.setAttribute("class", "container");
let sidebar = container.cloneNode(true);
let content = container.cloneNode(true);
let a = container.cloneNode(true);
let b = container.cloneNode(true);
let c = container.cloneNode(true);
let d = container.cloneNode(true);
a.style.position = "absolute";
a.style.left=px(5);
a.style.width = px(400);
a.style.height = px(100);
a.style.background = "rgba(0,0,0,0.5)";

Component("input0","input")
Component("input1","input")
Component("checkbox0","checkbox")
Component("textarea0","textarea")
Component("button0","button")
select("button0").textContent="Apply";

event(select("input0"),{
  type: "keydown",
  fn: function(elm,e){
    apply(filterChar(e.key))
  }
})
event(select("button0"),{
  type: "keydown",
  fn: function(elm,e){
    apply(filterChar(e.key))
  }
})
function styler(component,styles){
  for(let style in styles){
    component.style[style]=styles[style];
  }
}
/* experimental */
const absolute="absolute";
const click="click";

styler(select("checkbox0"),{ //chaining.................................................
  position: absolute,
  width: px(20),
  height: px(20),

})

event(select("checkbox0"), {
  type: click,
  fn: function(component, e){
    cl("clicked", component)// selected component > stateChange > select(otherComponent);
    // sequence option; Array.isArray() ? apply1 : applyMultiple
  }
})

function event(component,event){
  component.addEventListener(event.type, function(e){
    event.fn(component,e);
  })
}

b.style.position = "absolute";
b.style.left = px(410);
b.style.width = px(400);
b.style.height = px(200);
b.style.background = "rgba(0,0,0,0.5)";

let methods=Component("methods0", "ul");
apply(function NAMDSA(){})
toD(methods,getAppliedMethods().map(item => Li(item.name, "item")))

c.style.position = "absolute";
c.style.top = px(105);
c.style.left = px(5);
c.style.width = px(400);
c.style.height = px(400);
c.style.background = "rgba(0,0,0,0.5)";

d.style.position = "absolute";
d.style.top = px(205);
d.style.left = px(410);
d.style.width = px(400);
d.style.height = px(200);
d.style.background = "rgba(0,0,0,0.5)";

sidebar.setAttribute("class", "sidebar");
content.setAttribute("class", " content");

var main = [sidebar, content];
toD(a, ["filter:", 
          select("input0"),
          select("input1"), 
          select("checkbox0"),
          select("button0"), 
        ]);  //<-- cons.
toD(b, ["applied methods", methods]);
toD(c, [select("textarea0")]);
toD(d, ["used"]);
toD(sidebar, getList("a").map(item => Li(item, "item")));
toD(content, [a, b, c, d]);
toD(document.body, main);
